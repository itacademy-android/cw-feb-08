package itacademy.com.project007;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ArrayList<UserModel> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);

        arrayList = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            UserModel model = new UserModel();
            model.setName("John Smith");
            model.setCompany("Google company");
            arrayList.add(model);
        }

        UserAdapter adapter = new UserAdapter(this, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(getApplicationContext(), arrayList.get(position).getName() + " " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
