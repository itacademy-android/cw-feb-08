package itacademy.com.project007;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class UserAdapter extends ArrayAdapter<UserModel> {

    private Context context;

    public UserAdapter(@NonNull Context context, ArrayList<UserModel> arrayList) {
        super(context, 0, arrayList);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
       ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false);

            holder.imageView = convertView.findViewById(R.id.imageView);
            holder.tvName = convertView.findViewById(R.id.tvName);
            holder.tvCompany = convertView.findViewById(R.id.tvCompany);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        UserModel model = getItem(position);

        holder.imageView.setBackgroundResource(R.mipmap.ic_launcher);
        holder.tvName.setText(model.getName());
        holder.tvCompany.setText(model.getCompany());

        return convertView;
    }

    private class ViewHolder {
        TextView tvName;
        TextView tvCompany;
        ImageView imageView;
    }

}
